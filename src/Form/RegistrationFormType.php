<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\IsTrue;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, [
                'label'=> 'Nombre',
                'constraints' => [
                    new NotBlank([
                        'message' => 'Please escriba su nombre',
                        ]),
                    new Length([
                        'min' => 2,
                        'minMessage' => 'Su nombre debe contener al menos {{ limit }} carateres',
                        'max' => 30,
                        'maxMessage' => 'Su nombre debe contener máximo {{ limit }} carateres',
                        ]),
                    ],
            ])
            ->add('email', null, [
                'constraints' => [
                    new NotBlank([
                        'message' => 'Por favor escriba un password',
                        ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Su mail debe contener al menos {{ limit }} carateres',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                        ]),
                    ],
            ])
            ->add('password', PasswordType::class, [
                'mapped' => false,
                'label'=> 'Password',
                'constraints' => [
                    new NotBlank([
                    'message' => 'Please escriba un password',
                    ]),
                    new Length([
                        'min' => 6,
                        'minMessage' => 'Su password debe contener al menos {{ limit }} carateres',
                        // max length allowed by Symfony for security reasons
                        'max' => 4096,
                        ]),
                    ],
                ])
            ->add('agreeTerms', CheckboxType::class, [
                'mapped' => false,
                'label'=> 'Acepto Términos y Condiciones',
                'constraints' => [
                    new IsTrue([
                        'message' => 'Debes aceptar los términos.',
                    ]),
                ],
            ])
            ->add('save', SubmitType::class);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
