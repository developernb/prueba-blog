<?php

namespace App\Repository;

use App\Entity\ContactBlog;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ContactBlog|null find($id, $lockMode = null, $lockVersion = null)
 * @method ContactBlog|null findOneBy(array $criteria, array $orderBy = null)
 * @method ContactBlog[]    findAll()
 * @method ContactBlog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContactBlogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ContactBlog::class);
    }

    // /**
    //  * @return ContactBlog[] Returns an array of ContactBlog objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ContactBlog
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
