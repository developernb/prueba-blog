<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Post;
use App\Repository\PostRepository;
use App\Form\PostType;
use App\Service\FileUploader;

class BlogController extends AbstractController
{
    /**
     * @Route("/blog", name="blog")
     * @Route("/blog/mis-post", name="userPost")
     */
    public function index(Request $request)
    {
        $routeName = $request->attributes->get('_route');
        
        if (!$this->getUser() && $routeName == "userPost") {
            return $this->redirectToRoute('blog');
        }

        $entityManager = $this->getDoctrine()->getManager();

        $breadcrumb = "blog";
        if($routeName == "userPost"):
            $breadcrumb = "Mis Post";
            $posts = $entityManager->getRepository(Post::class)->findBy(array('userId' => $this->getUser()->getId(), 'publish' => true));
        else:
            $posts = $entityManager->getRepository(Post::class)->findPublish();
        endif;    


        return $this->render('blog/index.html.twig', [
            'posts' => $posts,
            'breadcrumb' => $breadcrumb
        ]);
    }
    
    /**
     * @Route("/blog/crear-post", name="addPost")
     * @Route("/blog/edit-post/{id}", name="editPost", requirements={"id"="\d+"})
     */
    public function managePost($id = null, Request $request, FileUploader $fileUploader)
    {
        if (!$this->getUser())
            return $this->redirectToRoute('blog');

        $entityManager = $this->getDoctrine()->getManager();
        $routeName = $request->attributes->get('_route');
        
        if ($routeName == "editPost"):
            $post = $entityManager->getRepository(Post::class)->find($id);
        else:
            $post = new Post();
        endif;    
        
        $form = $this->createForm(PostType::class, $post);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()):
            
            $user = $this->getUser();
            $post = $form->getData();
            $post->setUserId($user);
            
            $image = $form->get('image')->getData();

            if ($image) {
                $imageFileName = $fileUploader->upload($image);
                $post->setImage($imageFileName);
            }

            $entityManager->persist($post);
            $entityManager->flush();

            $this->addFlash('success', Post::FORM_OK);
            return $this->redirectToRoute('blog');
        endif;

        return $this->render('blog/add-post.html.twig', [
            'form' => $form->createView(),
            'title' => $routeName,
        ]);
    }

    /**
     * @Route("/blog/{id}", name="showPost", requirements={"id"="\d+"}))
     * 
     */
    public function showPost($id)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $post = $entityManager->getRepository(Post::class)->find($id);
        return $this->render('blog/show-post.html.twig', [
            'post' => $post,
        ]);
    }

    /**
     * @Route("/blog/delete-post/{id}", name="deletePost", requirements={"id"="\d+"})
     */
    public function deletePost($id)
    {
        if (!$this->getUser())
            return $this->redirectToRoute('blog');

        $entityManager = $this->getDoctrine()->getManager();
        
        $post = $entityManager->getRepository(Post::class)->find($id);

        if(!$post):
            $this->addFlash('warning', Post::NOT_EXIST);
            return $this->redirectToRoute('blog');
        endif;
        
        $post->setPublish(false);

        $entityManager->persist($post);
        $entityManager->flush();
        
        $this->addFlash('warning', Post::DELETE_OK);
        return $this->redirectToRoute('blog');
    }
}
